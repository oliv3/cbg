# ChatBladeGo

`cbg` is a command-line interface ( _CLI_ ) client for _OpenAI API_, written in _Golang_. It is a work in progress project that aims to provide a simple and efficient way to interact with _OpenAI_'s powerful language models. It tries to mimic the behaviour of the _python_ tool [chatblade](https://github.com/npiv/chatblade), but with much less dependencies.

## Build

You obviously need the `go` compiler to be installed, then simply:

```shell
$ make
```
This will create a binary called `cbg`

## Usage

Here are some examples of how to use it:

```shell
$ cbg -p my-prompt -sp -sq "What is the meaning of life?"
```

This command will use the `GPT-3.5 Turbo` model (default) to generate a response based on the _prompt_ `my-prompt` and the query "What is the meaning of life?". The _prompt_ must be located in `${HOME}/.config/cbg/prompts`. The _prompt_ can also be passed as an inlined parameter with the `-ip` flag.  
The `-sp` flag will show the prompt, and the `-sq` flag will show the query.  
Prompts can be listed with the `-lp` flag and a prompt can be edited or created using `-ep <prompt name>`.

```shell
$ cbg -c 4 "Hello, how are you?"
```

This command will use the `GPT-4` model to generate a response to the query "Hello, how are you?".

It is possible to pipe `stdout` to `cbg`:

```shell
$ cat vuln.c|cbg -p hacker "find a vulnerability in this C source file"
```

If markdown rendering is not wanted, use the `-r` (raw) flag.

To print an estimation of the query cost, use the `-t` flag.

```
$ cat main.go |cbg -r -t -p golang "make this go code better and more idiomatic"
```
| model | tokens | price |
|---|---|---|
| gpt-3.5-turbo | 1663 | 0.006524 |
| gpt-4 | 1663 | 0.145830 |
| gpt-4-32k | 1663 | 0.291660 |

In order to limit the number of tokens, for example when testing, you can use the `-m <tokens>` flag.

## CLI mode

`cbg` has a basic _CLI_ (`-i`, interactive) mode that supports all command line options, example:

```shell
$ cbg -i
gpt-3.5-turbo> show
loadprompt: sarcastic
prompt: in every answer you give, use a sarcastic tone or comment
query: 
model: gpt-3.5-turbo
maxtokens: 0
showprompt: false
showquery: false
interactive: true
dryrun: false
stream: false
raw: false
price: false
```
Every parameter which has a value can be changed this way:
```shell
gpt-3.5-turbo> maxtokens: 5
```
Boolean values can be changed this way:
```shell
gpt-3.5-turbo> showprompt
```
Here's a typical session
```shell
gpt-3.5-turbo> maxtokens: 5
gpt-3.5-turbo> prompt: whatever question is asked, you can only reply with the phrase "ok man"
gpt-3.5-turbo> say something

   result

Ok man.
```
To have the output streamed like in _ChatGPT_'s website, use the `-s` parameter.

## Sessions

`cbg` supports sessions, to recall last session simply add the `-l` flag. To name a session and use it, call `cbg` with the `-S` flag followed by a session name, if the name is `last`, it has the same effect as `-l`.

```shell
$ cbg -m 5 "from now on you shall only reply by two words sentences"

  result

  Understood, sure thing

$ cbg -l "say something"

  result

  Okay then.
```

With a named session

```shell
$ cbg -S 3words "say 3 words"

  result

  Love, Sunshine, Peace.

$ cbg -S 3words "what was my last question?"

  result

  Your last question was "Say 3 words".
```

By default, sessions are located in `${HOME}/.config/cbg/sessions`, this can be overridden by the `session` parameter in the configuration file.

Other session related flags are :

```shell
  -session-delete
        delete session
  -session-dump
        dump session to stdout
  -session-list
        list sessions
  -session-path
        path to sessions files
  -session-rename
        rename session
```

## Configuration

`cbg` uses an environment variable to configure the _OpenAI API_ key. You need to set the `OPENAI_API_KEY` environment variable to your _API_ key before using `cbg`. Alternatively, the _API_ key can be set in the configuration file located at `${HOME}/.config/cbg/config.yaml`:
```yaml
api-key: sk-xxx
```

As `cbg` uses [glamour](https://github.com/charmbracelet/glamour) for _markdown_ rendering, the style can be configured using the `GLAMOUR_STYLE` environment variable, see https://github.com/charmbracelet/glamour/tree/master/styles/gallery for the possible styles. Alternatively, style can be set in the configuration file:
```yaml
style: dracula
```
